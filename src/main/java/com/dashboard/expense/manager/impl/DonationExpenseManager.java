/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.manager.impl;

import com.dashboard.expense.manager.ExpenseManager;
import com.dashboard.expense.service.ExpenseManagerService;
import com.dashboard.expense.service.impl.DonationExpenseManagerServiceImpl;

/**
 * This class is just an example to show that we can have multiple implementations 
 * of the Service {@code ExpenseManager}
 * @author anand_raj
 *
 */
public class DonationExpenseManager implements ExpenseManager {

	/* (non-Javadoc)
	 * @see com.dashboard.expense.ExpenseManager#perform()
	 */
	@Override
	public void perform() {
		ExpenseManagerService<String,Integer> donationExpenseManager = new DonationExpenseManagerServiceImpl();
		donationExpenseManager.getInput();

	}

}
