/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.manager.impl;

import java.util.List;
import java.util.Map;

import com.dashboard.expense.manager.ExpenseManager;
import com.dashboard.expense.service.ExpenseManagerService;
import com.dashboard.expense.service.impl.TripExpenseManagerServiceImpl;
import com.dashboard.expense.utils.ErrorCodes;

/**
 * @author anand_raj
 *
 */
public class TripExpenseManager implements ExpenseManager {

	/**
	 * Creates {@code TripExpenseManagerServiceImpl} class 
	 * and makes a call to a series of method to 
	 * 1. Get the input from user
	 * 2. Calculate the Settlement details
	 * 3. Print the settlement details
	 */
	@Override
	public void perform() {
		ExpenseManagerService<String,Double> tripExpManagerService = new TripExpenseManagerServiceImpl();
		Map<String, Double> expenseInfo = tripExpManagerService.getInput(); 
		if(expenseInfo==null) {
			System.out.println("Please Rerun the program");
			System.exit(ErrorCodes.WRONG_INPUT);
		}
		List<String> settlementTransactions = tripExpManagerService.calculate(expenseInfo);
		tripExpManagerService.displayResults(settlementTransactions);
	}

}
