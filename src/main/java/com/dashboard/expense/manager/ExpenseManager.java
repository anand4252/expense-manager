/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.manager;

/**
 * @author anand_raj
 *
 */

public interface ExpenseManager {
	
	/**
	 * creates the correct service implementation of {@code ExpenseManagerService}
	 * and calls the appropriate methods as needed.
	 *
	 */
	public void perform();

}
