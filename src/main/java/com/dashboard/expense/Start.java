package com.dashboard.expense;

import com.dashboard.expense.enums.ExpenseType;
import com.dashboard.expense.manager.ExpenseManager;

/**
 * @author anand_raj
 *
 */
public class Start {

	
	/**There can be different types of Expense Manager like Trip, Donation etc. 
	 * This method is hard-coded for Trip. Please refer to {@code ExpenseType} for other options
	 * @param args
	 */
	public static void main(String[] args) {
		ExpenseManagerFactory expenseManagerFactory = new ExpenseManagerFactory();
		
		ExpenseManager expenseManager = expenseManagerFactory.getExpenseManager(ExpenseType.TRIP);
		expenseManager.perform();
	}

}
