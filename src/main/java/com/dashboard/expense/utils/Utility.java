/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.utils;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author anand_raj
 *
 */
public class Utility {
	
	private Utility() {
		
	}
	
	/**
	 * Sorts the {@code Map in ascending order based on values}
	 * @param amountOwedMap Map to be sorted
	 * @return
	 */
	public static Map<String, Double> sortMap(final Map<String, Double> amountOwedMap) {
		return amountOwedMap.entrySet().stream().sorted(comparingByValue())
				.collect(toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2, LinkedHashMap::new));
	}

}
