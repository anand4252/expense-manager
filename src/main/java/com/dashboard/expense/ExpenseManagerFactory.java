package com.dashboard.expense;

import com.dashboard.expense.enums.ExpenseType;
import com.dashboard.expense.manager.ExpenseManager;
import com.dashboard.expense.manager.impl.DonationExpenseManager;
import com.dashboard.expense.manager.impl.TripExpenseManager;
/**
 * @author anand_raj
 *
 */
public class ExpenseManagerFactory {

	public ExpenseManager getExpenseManager(final ExpenseType expenseType) {
		ExpenseManager expenseManager = null;
		switch (expenseType) {
		case TRIP:
			expenseManager = new TripExpenseManager();
			break;
		case DONATION:
			expenseManager = new DonationExpenseManager();
			break;
		default:
			break;
		}
		return expenseManager;
	}

}
