/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.enums;

/**
 * @author anand_raj
 *
 */
public enum ExpenseType {
	TRIP,
	DONATION
}
