package com.dashboard.expense.service;

import java.util.List;
import java.util.Map;

/**
 * Defines a Generic contract to calculate and manage the Expense Dashboard
 * 
 * @author anand_raj
 *
 */
public interface ExpenseManagerService<K, V> {

	/**
	 * Gets input details from the user and save the details in a {@code Map}
	 * 
	 * @return the Map that holds the input details
	 */
	public Map<K, V> getInput();

	/**
	 * Applies the expense logic on the user data to calculate the expense details
	 * 
	 * @param expenseInfo
	 *            Holds the input details
	 * @return {@code List} Holds the expense statement details
	 */
	public List<String> calculate(Map<K, V> expenseInfo);

	/**
	 * Prints the Expense Details
	 * 
	 * @param settlementTransactions
	 *            Holds the expense statement details
	 */
	public void displayResults(List<String> settlementTransactions);

}
