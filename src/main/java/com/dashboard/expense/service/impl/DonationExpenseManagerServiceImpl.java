/*
"(c) Walgreen Co.  All rights reserved"
*/
package com.dashboard.expense.service.impl;

import java.util.List;
import java.util.Map;

import com.dashboard.expense.service.ExpenseManagerService;

/**
 * This class is just an example to show that we can have multiple implementations 
 * of the Service {@code ExpenseManagerService}
 * @author anand_raj
 *
 */
public class DonationExpenseManagerServiceImpl implements ExpenseManagerService<String,Integer>  {

	/* (non-Javadoc)
	 * @see com.dashboard.expense.service.ExpenseManagerService#getInput()
	 */
	@Override
	public Map<String, Integer> getInput() {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see com.dashboard.expense.service.ExpenseManagerService#calculate(java.util.Map)
	 */
	@Override
	public List<String> calculate(Map<String, Integer> expenseInfo) {
		throw new UnsupportedOperationException();
	}

	/* (non-Javadoc)
	 * @see com.dashboard.expense.service.ExpenseManagerService#displayResults(java.util.List)
	 */
	@Override
	public void displayResults(List<String> settlementTransactions) {
		throw new UnsupportedOperationException();
	}

}
