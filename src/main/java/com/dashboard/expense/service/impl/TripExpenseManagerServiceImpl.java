package com.dashboard.expense.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.dashboard.expense.service.ExpenseManagerService;
import com.dashboard.expense.utils.Constants;
import com.dashboard.expense.utils.Utility;

/**
 * @author anand_raj
 *
 */
public class TripExpenseManagerServiceImpl implements ExpenseManagerService<String, Double> {

	/**
	 * 1. Gets the user data through command line 2. Aggregates the amount 3. Saves
	 * the final value in a {@code HashMap}
	 */
	@Override
	public Map<String, Double> getInput() {
		Map<String, Double> expenseMap = null;
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter the number of people that went for the trip[NUMBERS ONLY]:");
			int count = sc.nextInt();
			int size = (int) Math.ceil(1 + count / 0.75); // To prevent Rehashing of HashMap;
			expenseMap = new HashMap<>(size);
			for (int i = 1; i <= count; i++) {
				String isSamePerson = "y";
				while ("y".equalsIgnoreCase(isSamePerson)) {
					System.out.println("Enter expense details of person no " + i + " in the format: Name,Amount Spent");
					String expenseDetails = sc.next();
					String[] expenseSplit = expenseDetails.split(",");
					String name = expenseSplit[0].toUpperCase();
					if (expenseMap.containsKey(name)) {
						Double sum = expenseMap.get(name);
						sum = sum + Double.parseDouble(expenseSplit[1]);
						expenseMap.put(name, sum);
					} else {
						expenseMap.put(name, Double.parseDouble(expenseSplit[1]));
					}
					System.out.println("Do you want to enter another expense for the same person? Y/N");
					isSamePerson = sc.next();
				}
			}
		} catch (Exception e) {
			System.out.println("Please provide the right input in the mentioned format");
			expenseMap = null;
		}
		return expenseMap;
	}

	@Override
	public List<String> calculate(final Map<String, Double> expenseInfo) {
		List<String> settlementTransactions;
		double totalAmount = getTotalMoneySpent(expenseInfo);
		double amountPerHead = totalAmount / expenseInfo.size();

		Map<String, Double> borrowerMap = new HashMap<>();
		for (Map.Entry<String, Double> expense : expenseInfo.entrySet()) {
			borrowerMap.put(expense.getKey(), amountPerHead - expense.getValue());
		}

		Map<String, Double> lenderMap = getLenderMap(borrowerMap);
		if (lenderMap.size() == 0) {
			settlementTransactions = new ArrayList<>(1);
			settlementTransactions.add("Noone owes anyone");
			return settlementTransactions;
		}

		Map<String, Double> sortedBorrowerMap = Utility.sortMap(borrowerMap);
		Map<String, Double> sortedLenderMapMap = Utility.sortMap(lenderMap);

		settlementTransactions = applyExpenseSharingLogic(sortedBorrowerMap, sortedLenderMapMap);

		return settlementTransactions;
	}

	/**
	 * This method sums up all the money Spent by each individual in the Trip
	 * 
	 * @param expenseInfo
	 * @return
	 */
	private double getTotalMoneySpent(final Map<String, Double> expenseInfo) {
		double totalAmount = 0;
		for (Map.Entry<String, Double> expense : expenseInfo.entrySet()) {
			totalAmount += expense.getValue();
		}
		return totalAmount;
	}

	/**
	 * Prints the final value
	 */
	@Override
	public void displayResults(final List<String> settlementTransactions) {
		for (String settlementTransaction : settlementTransactions) {
			System.out.println(settlementTransaction);
		}
	}

	/**
	 * Applies the core expense sharing logic
	 * 
	 * @param sortedBorrowerMap
	 * @param sortedLenderMap
	 * @return
	 */
	private List<String> applyExpenseSharingLogic(final Map<String, Double> sortedBorrowerMap,
			final Map<String, Double> sortedLenderMap) {
		List<String> settlementTransactions = new ArrayList<>();
		for (Iterator<Map.Entry<String, Double>> borrowerIterator = sortedBorrowerMap.entrySet()
				.iterator(); borrowerIterator.hasNext();) {
			Map.Entry<String, Double> borrowerEntry = borrowerIterator.next();
			for (Iterator<Map.Entry<String, Double>> lenderIterator = sortedLenderMap.entrySet()
					.iterator(); lenderIterator.hasNext();) {
				Map.Entry<String, Double> lenderEntry = lenderIterator.next();
				if (borrowerEntry.getValue() < (lenderEntry.getValue() * -1)) {
					settlementTransactions.add(borrowerEntry.getKey() + Constants.OWES + lenderEntry.getKey() + " "
							+ borrowerEntry.getValue());
					sortedLenderMap.put(lenderEntry.getKey(), lenderEntry.getValue() + borrowerEntry.getValue());
					borrowerIterator.remove();
					break;
				} else if (borrowerEntry.getValue() > (lenderEntry.getValue() * -1)) {
					settlementTransactions.add(borrowerEntry.getKey() + Constants.OWES + lenderEntry.getKey() + " "
							+ lenderEntry.getValue() * -1);
					sortedBorrowerMap.put(borrowerEntry.getKey(), lenderEntry.getValue() + borrowerEntry.getValue());
					lenderIterator.remove();
				} else {
					settlementTransactions.add(borrowerEntry.getKey() + Constants.OWES + lenderEntry.getKey() + " "
							+ lenderEntry.getValue() * -1);
					borrowerIterator.remove();
					lenderIterator.remove();
				}
			}
		}
		return settlementTransactions;
	}

	/**
	 * Creates a {@code HashMap} that holds the Lender details.
	 * 
	 * @param amountOwedMap
	 * @return
	 */
	private Map<String, Double> getLenderMap(final Map<String, Double> amountOwedMap) {
		Map<String, Double> lenderMap = new HashMap<>();
		Iterator<Map.Entry<String, Double>> iterator = amountOwedMap.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, Double> expenseEntry = iterator.next();
			if (expenseEntry.getValue() < 0) {
				lenderMap.put(expenseEntry.getKey(), expenseEntry.getValue());
				iterator.remove();
			}
		}
		return lenderMap;
	}

}
