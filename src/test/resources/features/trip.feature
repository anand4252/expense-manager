Feature: Trip Expense Manager

#--------------------------------------------------------------------------------------------------------
@TripExpense-1
  Scenario Outline: To check if amounts are settled between 3 person
    Given Persons "<persons>" Amounts "<amounts>"
     When the service is invoked
     Then the output should be "<transaction>"
  
    Examples: 
      | persons             | amounts                   | transaction                                                     | 
      | prema,anand         | 400.0,600.0               | prema owes anand 100.0                                          | 
      | prema,appu,anand    | 600.0,1200.0,300.0        | prema owes appu 100.0,anand owes appu 400.0                     | 
      | prema,appu,anand    | 100.0,1300.0,1000.0       | prema owes appu 500.0,prema owes anand 200.0                    | 
      | prema,appu,anand,rt | 100.0,1300.0,1000.0,400.0 | rt owes appu 300.0,prema owes appu 300.0,prema owes anand 300.0 | 
      | prema,appu,anand    | 600.0,600.0,600.0         | Noone owes anyone                                               | 
      | prema,appu          | 600.0,600.0               | Noone owes anyone                                               | 
      | prema,appu,anand,rt | 500.0,500.0,500.0,500.0   | Noone owes anyone                                               | 
      | prema,appu,anand    | 0.0,0.0,0.0               | Noone owes anyone                                               | 
  #--------------------------------------------------------------------------------------------------------
  
