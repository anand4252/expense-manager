package com.dashboard.expense;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * @author anand_raj
 *
 */
@RunWith(Cucumber.class) 
@CucumberOptions(features = "classpath:features", plugin = { "pretty", "json:build/reports/cucumber/cucumber-json-report.json" }, glue = {
"com.dashboard.expense.stepdefs" })
public class RunTest {

}
