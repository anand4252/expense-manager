package com.dashboard.expense.stepdefs;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dashboard.expense.service.ExpenseManagerService;
import com.dashboard.expense.service.impl.TripExpenseManagerServiceImpl;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Step definition class to test multiple test scenarios mentioned in
 * {@code trip.feature}
 * 
 * @author anand_raj
 *
 */
public class TripSteps {

	private ExpenseManagerService<String, Double> expenseManagerService;
	private Map<String, Double> expenseInfo;

	private List<String> settlementTransactions;

	@Before
	public void initialSetup() {
		expenseManagerService = new TripExpenseManagerServiceImpl();
		expenseInfo = new HashMap<>();
	}

	@Given("^Persons \"([^\"]*)\" Amounts \"([^\"]*)\"$")
	public void setUserInputs(String persons, String amounts) throws Throwable {
		String personArr[] = persons.split(",");
		String amountArr[] = amounts.split(",");
		for (int i = 0; i < personArr.length; i++) {
			expenseInfo.put(personArr[i], Double.parseDouble(amountArr[i]));
		}
		System.out.println("expenseInfo: " + expenseInfo);

	}

	@When("^the service is invoked$")
	public void invokeService() throws Throwable {
		settlementTransactions = this.expenseManagerService.calculate(expenseInfo);
		System.out.println(settlementTransactions);
	}

	@Then("^the output should be \"([^\"]*)\"$")
	public void verifyOutput(String transactions) throws Throwable {
		String[] transactionArray = transactions.split(",");
		for (int i = 0; i < transactionArray.length; i++) {
			assertTrue(transactionArray[i].equalsIgnoreCase(settlementTransactions.get(i).trim()));
		}
	}
}
